# Bevelled edge

If you want your polygons to have a bevelled edge, you can use Geometry Generators to do so.  In this example I have added a bevelled edge inside of the polygon, so the polygon remains the same size.

<table><tr><td><a href="https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/bevelled_edge/bevelled_edge.qml?inline=false"><img src="../../Example_images/bevelled_edge.png"></a></td></tr></table>

[Download the QML file for this Geometry Generator Style](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/bevelled_edge/bevelled_edge.qml?inline=false)
