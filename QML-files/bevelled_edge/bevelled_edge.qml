<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis styleCategories="Symbology" version="3.22.1-Białowieża">
  <renderer-v2 type="singleSymbol" enableorderby="0" referencescale="-1" forceraster="0" symbollevels="0">
    <symbols>
      <symbol name="0" type="fill" alpha="1" clip_to_extent="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" locked="0" enabled="1" pass="0">
          <Option type="Map">
            <Option name="border_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="color" type="QString" value="221,221,221,255"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="138,138,138,255"/>
            <Option name="outline_style" type="QString" value="no"/>
            <Option name="outline_width" type="QString" value="0.26"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="style" type="QString" value="solid"/>
          </Option>
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="221,221,221,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="138,138,138,255"/>
          <prop k="outline_style" v="no"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer class="GeometryGenerator" locked="0" enabled="1" pass="0">
          <Option type="Map">
            <Option name="SymbolType" type="QString" value="Fill"/>
            <Option name="geometryModifier" type="QString" value=" difference(  $geometry, buffer($geometry, -100))"/>
            <Option name="units" type="QString" value="MapUnit"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v=" difference(  $geometry, buffer($geometry, -100))"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
          <symbol name="@0@1" type="fill" alpha="1" clip_to_extent="1" force_rhr="0">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" type="QString" value=""/>
                <Option name="properties"/>
                <Option name="type" type="QString" value="collection"/>
              </Option>
            </data_defined_properties>
            <layer class="ShapeburstFill" locked="0" enabled="1" pass="0">
              <Option type="Map">
                <Option name="blur_radius" type="QString" value="0"/>
                <Option name="color" type="QString" value="106,106,106,255"/>
                <Option name="color1" type="QString" value="69,116,40,255"/>
                <Option name="color2" type="QString" value="188,220,60,255"/>
                <Option name="color_type" type="QString" value="0"/>
                <Option name="discrete" type="QString" value="0"/>
                <Option name="distance_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
                <Option name="distance_unit" type="QString" value="MM"/>
                <Option name="gradient_color2" type="QString" value="221,221,221,255"/>
                <Option name="ignore_rings" type="QString" value="1"/>
                <Option name="max_distance" type="QString" value="4"/>
                <Option name="offset" type="QString" value="0,0"/>
                <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
                <Option name="offset_unit" type="QString" value="MM"/>
                <Option name="rampType" type="QString" value="gradient"/>
                <Option name="use_whole_shape" type="QString" value="0"/>
              </Option>
              <prop k="blur_radius" v="0"/>
              <prop k="color" v="106,106,106,255"/>
              <prop k="color1" v="69,116,40,255"/>
              <prop k="color2" v="188,220,60,255"/>
              <prop k="color_type" v="0"/>
              <prop k="discrete" v="0"/>
              <prop k="distance_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="distance_unit" v="MM"/>
              <prop k="gradient_color2" v="221,221,221,255"/>
              <prop k="ignore_rings" v="1"/>
              <prop k="max_distance" v="4"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="rampType" v="gradient"/>
              <prop k="use_whole_shape" v="0"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" type="QString" value=""/>
                  <Option name="properties"/>
                  <Option name="type" type="QString" value="collection"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerGeometryType>2</layerGeometryType>
</qgis>
