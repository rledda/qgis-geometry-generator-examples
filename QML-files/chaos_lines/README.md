# Chaos lines
If you prefer your maps chaotic, this is your style.  The example creates a curly line between the center of your screen and each point.

<table><tr><td><a href="https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/chaos_lines/chaos_lines.qml?inline=false"><img src="../../Example_images/chaos_lines.png"></a></td></tr></table> 

[Download the QML file for this Geometry Generator Style](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/chaos_lines/chaos_lines.qml?inline=false)
