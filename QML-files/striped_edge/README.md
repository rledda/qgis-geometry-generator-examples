# Striped edge

If you want your polygons to have a striped edge, you can use Geometry Generators to do so.  In this example I have added a bevelled edge outside of the polygon, so the result is bigger than the original polygon.

<table><tr><td><a href="https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/striped_edge/striped_edge.qml?inline=false"><img src="../../Example_images/striped_edge.png"></a></td></tr></table>

[Download the QML file for this Geometry Generator Style](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/striped_edge/striped_edge.qml?inline=false)
