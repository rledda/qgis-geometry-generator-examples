# Gradient border
This simple QGIS style changes the border of polygons to a gradient. It uses a negative buffer to create the border, so this means the outside of the polygon remains the same.  You could also make a double sided buffer, but that would change the outline of the polygon.  I also created an animated version for QGIS 3.26 and later.

Of course you can make a similar style for polyline layers using the same technique.

<table><tr><td><a href="https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/gradient_border/gradient_border.qml?inline=false"><img src="../../Example_images/gradient_border.png"></a></td></tr></table> 

[Download the QML file for this Geometry Generator Style](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/gradient_border/gradient_border.qml?inline=false)

[Download the QML file for the animated Geometry Generator Style](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/gradient_border/gradient_border_animated.qml?inline=false)
