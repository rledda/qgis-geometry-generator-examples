<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis readOnly="0" styleCategories="LayerConfiguration|Symbology" version="3.26.0-Buenos Aires">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <renderer-v2 type="singleSymbol" forceraster="0" enableorderby="0" symbollevels="0" referencescale="-1">
    <symbols>
      <symbol type="fill" name="0" frame_rate="120" force_rhr="0" clip_to_extent="1" is_animated="1" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="GeometryGenerator" pass="0" enabled="1" locked="0">
          <Option type="Map">
            <Option type="QString" name="SymbolType" value="Fill"/>
            <Option type="QString" name="geometryModifier" value=" difference( $geometry, buffer($geometry, -50))"/>
            <Option type="QString" name="units" value="MapUnit"/>
          </Option>
          <prop v="Fill" k="SymbolType"/>
          <prop v=" difference( $geometry, buffer($geometry, -50))" k="geometryModifier"/>
          <prop v="MapUnit" k="units"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
          <symbol type="fill" name="@0@0" frame_rate="10" force_rhr="0" clip_to_extent="1" is_animated="0" alpha="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </data_defined_properties>
            <layer class="GradientFill" pass="0" enabled="1" locked="0">
              <Option type="Map">
                <Option type="QString" name="angle" value="90"/>
                <Option type="QString" name="color" value="0,0,255,255"/>
                <Option type="QString" name="color1" value="69,116,40,255"/>
                <Option type="QString" name="color2" value="188,220,60,255"/>
                <Option type="QString" name="color_type" value="0"/>
                <Option type="QString" name="coordinate_mode" value="0"/>
                <Option type="QString" name="direction" value="ccw"/>
                <Option type="QString" name="discrete" value="0"/>
                <Option type="QString" name="gradient_color2" value="255,8,8,255"/>
                <Option type="QString" name="offset" value="0,0"/>
                <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                <Option type="QString" name="offset_unit" value="MM"/>
                <Option type="QString" name="rampType" value="gradient"/>
                <Option type="QString" name="reference_point1" value="0.5,0"/>
                <Option type="QString" name="reference_point1_iscentroid" value="0"/>
                <Option type="QString" name="reference_point2" value="0.5,1"/>
                <Option type="QString" name="reference_point2_iscentroid" value="0"/>
                <Option type="QString" name="spec" value="rgb"/>
                <Option type="QString" name="spread" value="0"/>
                <Option type="QString" name="type" value="0"/>
              </Option>
              <prop v="90" k="angle"/>
              <prop v="0,0,255,255" k="color"/>
              <prop v="69,116,40,255" k="color1"/>
              <prop v="188,220,60,255" k="color2"/>
              <prop v="0" k="color_type"/>
              <prop v="0" k="coordinate_mode"/>
              <prop v="ccw" k="direction"/>
              <prop v="0" k="discrete"/>
              <prop v="255,8,8,255" k="gradient_color2"/>
              <prop v="0,0" k="offset"/>
              <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
              <prop v="MM" k="offset_unit"/>
              <prop v="gradient" k="rampType"/>
              <prop v="0.5,0" k="reference_point1"/>
              <prop v="0" k="reference_point1_iscentroid"/>
              <prop v="0.5,1" k="reference_point2"/>
              <prop v="0" k="reference_point2_iscentroid"/>
              <prop v="rgb" k="spec"/>
              <prop v="0" k="spread"/>
              <prop v="0" k="type"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" name="name" value=""/>
                  <Option type="Map" name="properties">
                    <Option type="Map" name="angle">
                      <Option type="bool" name="active" value="true"/>
                      <Option type="QString" name="expression" value="@symbol_frame % 360"/>
                      <Option type="int" name="type" value="3"/>
                    </Option>
                  </Option>
                  <Option type="QString" name="type" value="collection"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <previewExpression>"fid"</previewExpression>
  <layerGeometryType>2</layerGeometryType>
</qgis>
