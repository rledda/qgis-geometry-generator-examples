<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis styleCategories="Symbology" version="3.24.3-Tisler">
  <renderer-v2 type="singleSymbol" enableorderby="0" referencescale="-1" symbollevels="0" forceraster="0">
    <symbols>
      <symbol type="fill" name="0" clip_to_extent="1" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" locked="0" class="GeometryGenerator" enabled="1">
          <Option type="Map">
            <Option type="QString" name="SymbolType" value="Fill"/>
            <Option type="QString" name="geometryModifier" value=" difference( $geometry, buffer($geometry, -50))"/>
            <Option type="QString" name="units" value="MapUnit"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v=" difference( $geometry, buffer($geometry, -50))"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
          <symbol type="fill" name="@0@0" clip_to_extent="1" force_rhr="0" alpha="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </data_defined_properties>
            <layer pass="0" locked="0" class="GradientFill" enabled="1">
              <Option type="Map">
                <Option type="QString" name="angle" value="90"/>
                <Option type="QString" name="color" value="0,0,255,255"/>
                <Option type="QString" name="color1" value="69,116,40,255"/>
                <Option type="QString" name="color2" value="188,220,60,255"/>
                <Option type="QString" name="color_type" value="0"/>
                <Option type="QString" name="coordinate_mode" value="0"/>
                <Option type="QString" name="direction" value="ccw"/>
                <Option type="QString" name="discrete" value="0"/>
                <Option type="QString" name="gradient_color2" value="255,8,8,255"/>
                <Option type="QString" name="offset" value="0,0"/>
                <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                <Option type="QString" name="offset_unit" value="MM"/>
                <Option type="QString" name="rampType" value="gradient"/>
                <Option type="QString" name="reference_point1" value="0.5,0"/>
                <Option type="QString" name="reference_point1_iscentroid" value="0"/>
                <Option type="QString" name="reference_point2" value="0.5,1"/>
                <Option type="QString" name="reference_point2_iscentroid" value="0"/>
                <Option type="QString" name="spec" value="rgb"/>
                <Option type="QString" name="spread" value="0"/>
                <Option type="QString" name="type" value="0"/>
              </Option>
              <prop k="angle" v="90"/>
              <prop k="color" v="0,0,255,255"/>
              <prop k="color1" v="69,116,40,255"/>
              <prop k="color2" v="188,220,60,255"/>
              <prop k="color_type" v="0"/>
              <prop k="coordinate_mode" v="0"/>
              <prop k="direction" v="ccw"/>
              <prop k="discrete" v="0"/>
              <prop k="gradient_color2" v="255,8,8,255"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="rampType" v="gradient"/>
              <prop k="reference_point1" v="0.5,0"/>
              <prop k="reference_point1_iscentroid" v="0"/>
              <prop k="reference_point2" v="0.5,1"/>
              <prop k="reference_point2_iscentroid" v="0"/>
              <prop k="spec" v="rgb"/>
              <prop k="spread" v="0"/>
              <prop k="type" v="0"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" name="name" value=""/>
                  <Option name="properties"/>
                  <Option type="QString" name="type" value="collection"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerGeometryType>2</layerGeometryType>
</qgis>
